package com.epam;

import com.epam.controller.Rieltor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private final static Logger logger  = LogManager.getLogger(Main.class);
    public static void main(String[] args) {

        logger.debug("Debugging");
        logger.error("error");
        logger.warn("warn");
        logger.fatal("fatal");
        logger.info("info");
        logger.trace("trace");

    }
}
